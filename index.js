const fetch = require('node-fetch');
const crypto = require("crypto");
var path = require('path');
const util = require("util");
const query = require('micro-query');

const privKeyFile = "config/priv.key";

module.exports = async (request, response) => {

  let url = query(request).url

  if (url == undefined) {
    return "Error: No url in request query defined."
  }

	const res = await fetch(url);
	const json = await res.json();

  var fs = require('fs');

  var timestamp = Math.round((new Date()).getTime() / 1000);

  var json_string = JSON.stringify(json)

  var dirname = 'snapshots/'

  // if nno directory exits, create it
  if (!fs.existsSync(dirname)){
      fs.mkdirSync(dirname);
      console.log("snapshots folder not exits - created it!");
  }
  var file_name = dirname + timestamp + '.json'

  fs.writeFile(file_name, json_string, function (err) {
    if (err) throw err;
    console.log('Saved!');
  });


  // Creates and returns a Verify object that uses RSA-SHA256
  //https://en.wikipedia.org/wiki/RSA_(cryptosystem)

  // https://en.wikipedia.org/wiki/RSA_(cryptosystem)
  // https://en.wikipedia.org/wiki/Secure_Hash_Algorithms
  const signer = crypto.createSign("RSA-SHA256");

  // Updates the Verify content with the given data
  signer.update(json_string);

  // Calculates the signature on the json data with the private key and returns the signature in hex as a string
  // https://nodejs.org/api/crypto.html#crypto_sign_sign_privatekey_outputformat
  const file = await util.promisify(fs.readFile)(path.join(privKeyFile));


  const sign = signer.sign(file.toString(), "hex");


  const iotaLibrary = require('@iota/core')
  const Converter = require('@iota/converter')

  const iota = iotaLibrary.composeAPI({
    provider: 'https://nodes.devnet.thetangle.org:443'
  })

  // Use a random seed as there is no tokens being sent.
  const seed =
    'PUEOTSEITFEVEWCWBTSIZM9NKRGJEIMXTULBACGFRQK9IMGICLBKW9TTEVSDQMGWKBXPVCBMMCXWMNPEI'

  // Create a variable for the address we will send too
  const address =
    'O9YHEOPRUXCPNHJVFNMGYSOBWLEFBWSZKMIQVOBCWSLWKVKEYTIIFTFCYFTIFHKWVWTIRJRDILFBKTWGX'

  const link = request.headers.referer + file_name
  const object = {
    data_url: link,
    timestamp: timestamp,
    sign: sign
  }

  const message = Converter.asciiToTrytes(JSON.stringify(object))

  const transfers = [
    {
      value: 0,
      address: address, // Where the data is being sent
      message: message // The message converted into trytes
    }
  ]

  let bundleHash = iota
    .prepareTransfers(seed, transfers)
    .then(trytes => iota.sendTrytes(trytes, 3, 9))
    .then(bundle => {
      console.log('Transfer successfully sent')
      console.log(bundle[0].bundle);
      return bundle[0].bundle
    })
    .catch(err => {
      console.log(err)
    })

	return bundleHash;
}

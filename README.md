# Snapshot Service

This Services takes a snapshot of a json object which will be returned by an provided API endpoint.

* saves the json object in a file named with the created timpstamp
* stores the signed object with metadata in the tangle (url, timestamp)



### API:

* get "/", returns all snapshots
* get "/<timestamp>" returns the data for the snapshot with the given timestamp
* post "/", creates a snapshot and returns the snapshot and the snapshot data

#### example snapshot

http://localhost:3002/?url=https://nutzdoch.einfachiota.de/nodes

snapshot = {
  timestamp: 123456,
  data_url: "http://piri-snapshots.einfachiota.de/snapshot/123456",
  sign: "cool-signature",
  bundleHash: "EGJYIWRYVYIVXNVF9XQ9SXYGJEESWVVDTZYDDQECLIINIDZAECDIBHFLSEFUCHQYZPZVZCKRULVYWLBHZ"
}

### Setup:

* npm install

* npm run start


### TODO

* create keypair if not exit

* create snapshots direcotry if not exit

* validate query url
